
class State
  attr_reader  :label, :type

  def initialize( state_label, state_type=:standard )  
    @label = state_label  
    if state_type.nil?
      @type = :standard
    else
      @type = state_type.downcase.to_sym
    end
  end

  def to_liquid
  {
    'label'=>self.label,
    'type'=>self.type.to_s 
  }
  end
end