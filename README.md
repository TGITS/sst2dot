# README

## Presentation

**sst2dot** stands for **State Transition Table to Dot**. 

It's a little tool in order to convert a _State Transition Table_ specified in a CSV file in a _Graphviz dot_ file that can be in turn transformed in a graph image with Graphviz binaries.
The objectives are the following :

* a small utility program to do and learn some Ruby
* a small tool to ease the creation of State Chart

The inspiration is a small paragraph in a book on UML 2 par Martin Fowler.

The inputs of the tool are 2 _CSV_ files and it's output is a _dot_ file that can be processed by GraphViz. 

## Input CSV format for the STT file

The expected STT CSV format is the following :

```
source state,target state,event,guard,action
START,A,event_0,,
A,B,event_1,guard_1,action_1
B,C,event_2,guard_2,action_2
B,D,event_3,,action_3
B,E,event_4,,
C,F,event_5,,
D,F,event_6,,
F,END,event_7,,
E,END,event_8,,
```

* The first line is a header line that is not processed.
* The CSV file must have 5 columns
* The columns for specifying the event, the guard and the action can be empty

The first column is to indicate the source state, the second the target state, the third the event that cause the transition from the source to the target state, the fourth the logical guard that must be true for the transition to really occur and the fifth the action that is carried out if the transition occurs.

The initial and final states can have any name but this name will not be displayed.

The expected states list CSV format is the following :

```
state,type
START,initial
A,standard
B,standard
C,
D,
E,
F,
END,final
```

* The first line is a header line that is not processed.
* The CSV file must have 2 columns
* The second column (for the type) can be empty ; an empty column for the type is equivalent to a _standard_ type. 

The first argument given to the script is the filename (complete path) of the STT CSV file, the second argument is the state list CSV File and the third argument is the name of the ouput file in the dot format.

To run the application (thereafter with jruby)
``` 
jruby stt2dot.rb stt_file.csv states_files.csv result_file.dot
``` 
## Todo

* Correctly commenting the code (cf. Eloquent Ruby)
* Completing the Rakefile to execute the test and the example
* Creating a gem for this program

