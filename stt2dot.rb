# Author: Christophe Vaudry
# Copyright 2014 Christophe Vaudry
# 
#

require 'reader'
require 'generator'

if ARGV.length < 3
  puts "You must provide 3 arguments : the stt file name, the states list file name and the output 'dot' file name"
else
  stt_filename = ARGV[0]
  states_filename = ARGV[1]
  output_filename = ARGV[2]

  reader = Reader.new
  created_states = reader.get_states_from_file(states_filename)
  edges_list = reader.get_edges_from_file(stt_filename, created_states)

  generator = Generator.new
  generator.generate(created_states, edges_list, output_filename)
end