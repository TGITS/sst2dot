require 'liquid'

class Generator
  def generate(created_states, edges_list,output_filename)
    template = Liquid::Template.parse(File.read("graphviz_output.template"))
    File.open(output_filename,mode="w") do |file|
      file << template.render({'states' => created_states, 'edges' => edges_list})
    end
  end
end