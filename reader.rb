require 'csv'
require 'state'
require 'edge'

class Reader
    
  def get_states_from_file(filename)
    states_rows =  CSV.read(filename, { headers: true, converters: :numeric, header_converters: :symbol })
    created_states = Hash.new
    states_rows.each do |row|
      label = row.field(0)
      type = row.field(1)
      label_sym = label.to_sym
      if created_states[label_sym].nil?
        source = State.new(label,type)
        created_states[label_sym] = source
      end
    end    
    return created_states
  end

  def get_edges_from_file(filename, created_states)
    stt_rows = CSV.read(filename, { headers: true, converters: :numeric, header_converters: :symbol })
    edges_list = Array.new
    stt_rows.each do |row|
      source_label = row.field(0)

      if created_states[source_label.to_sym].nil?
        puts "state #{source_label} declared in stt but not in the state list"
      else 
        source = created_states[source_label.to_sym]
      end

      target_label = row.field(1)
            
      if created_states[target_label.to_sym].nil?
        puts "state #{source_label} declared in stt but not in the state list"
      else 
        target = created_states[target_label.to_sym]
      end
    
      edge = Edge.new(source,target,row.field(2),row.field(3),row.field(4))
      edges_list << edge
    end
    return edges_list
  end

end