require 'test/unit'
require 'edge'
require 'state'

class TestEdge < Test::Unit::TestCase

	def test_create_complete_edge
		start_state = State.new("START")
		end_state = State.new("END")

		edge = Edge.new(start_state, end_state, "event", "guard", "action")

		assert_equal(false, edge.no_event?)
		assert_equal(false, edge.no_guard?)
		assert_equal(false, edge.no_action?)
	end

	def test_create_minimal_edge
		start_state = State.new("START")
		end_state = State.new("END")

		edge = Edge.new(start_state, end_state)

		assert_equal(true, edge.no_event?)
		assert_equal(true, edge.no_guard?)
		assert_equal(true, edge.no_action?)
	end

end