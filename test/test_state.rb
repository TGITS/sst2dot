require 'test/unit'
require 'state'

class TestState < Test::Unit::TestCase
	
	def test_create_start_state
		start_state_1 = State.new("START","initial")
		assert_equal(:initial,start_state_1.type)
		assert_equal("START",start_state_1.label)
		
		start_state_2 = State.new("start","INITIAL");
		assert_equal(:initial,start_state_2.type)
		assert_equal("start",start_state_2.label)

		not_start_state = State.new("Not_START")
		assert_not_equal(:initial,not_start_state.type)
		assert_not_equal(:final,not_start_state.type)
		assert_equal(:standard,not_start_state.type)
	end

	def test_create_end_state
		end_state_1 = State.new("END","final")
		assert_equal(:final,end_state_1.type)
		assert_equal("END",end_state_1.label)
		
		end_state_2 = State.new("end","FINAL");
		assert_equal(:final,end_state_2.type)
		assert_equal("end",end_state_2.label)

		not_end_state = State.new("Not_END")
		assert_not_equal(:initial,not_end_state.type)
		assert_not_equal(:final,not_end_state.type)
		assert_equal(:standard,not_end_state.type)
	end

	def test_create_standard_state
		end_state_1 = State.new("END", "final")
		assert_equal(:final,end_state_1.type)
		assert_equal("END",end_state_1.label)
		
		end_state_2 = State.new("end", "final")
		assert_equal(:final,end_state_2.type)
		assert_equal("end",end_state_2.label)

		not_end_state = State.new("Not_END")
		assert_not_equal(:initial,not_end_state.type)
		assert_not_equal(:final,not_end_state.type)
		assert_equal(:standard,not_end_state.type)
		assert_equal("Not_END",not_end_state.label)

		standard_state = State.new("standard","standard")
		assert_not_equal(:initial,standard_state.type)
		assert_not_equal(:final,standard_state.type)
		assert_equal(:standard,standard_state.type)
		assert_equal("standard",standard_state.label)
	end

end
