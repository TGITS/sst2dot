class Edge

  attr_reader  :source, :target, :event, :guard, :action
  
  def initialize(source, target, event="", guard="", action="")
    @source = source
    @target = target
    @event  = event.nil? ? "" : event.strip
    @guard  = guard.nil? ? "" : guard.strip
    @action = action.nil? ? "" : action.strip
  end

  def no_event?
    @event.empty?
  end

  def event?
    !self.no_event?
  end

  def no_guard?
    @guard.empty?
  end

  def guard?
    !self.no_guard?
  end

  def no_action?
    @action.empty?
  end

  def action?
    !self.no_action?
  end

  def to_liquid
  {
    'source'  => self.source,
    'target'  => self.target,
    'event'   => self.event,
    'event?'  => self.event?,
    'guard'   => self.guard,
    'guard?'  => self.guard?,
    'action'  => self.action,
    'action?' => self.action?
  }
  end

end